import azure.functions as func
import logging
import pyodbc
import random

# Function used to generate random numbers
def generate_discrete_number(lower_limit, upper_limit):
    return int(random.triangular(lower_limit, upper_limit))

def get_temperature():
    return generate_discrete_number(
        8,
        15,
    )


def get_wind():
    return generate_discrete_number(
        15,
        25,
    )


def get_humidity():
    return generate_discrete_number(
        40,
        70,
    )


def get_co2():
    return generate_discrete_number(
        500,
        1500,
    )


def get_sensor_data(id):
    return {
        "Temperature": get_temperature(),
        "Wind": get_wind(),
        "Humidity": get_humidity(),
        "CO2": get_co2(),
    }

# Writing generated sensor data to database
def write_data():
    connection_string = "Driver={ODBC Driver 18 for SQL Server};Server=tcp:sc21sc.database.windows.net,1433;Database=sensors;Uid=CloudSA8960402f;Pwd={password};Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;"

    try:
        conn = pyodbc.connect(connection_string)
        cursor = conn.cursor()

        cursor.execute(f"SELECT COUNT(*) FROM [sensor-data];")
        row = cursor.fetchone()
        count = 0
        if row:
            count = row[0]
        if count >= 10000:
            cursor.execute(f"DELETE FROM [sensor-data];")

        for x in range(0, 20):
            sensor_data = get_sensor_data(x)
            cursor.execute(
                f"INSERT INTO [sensor-data] (sensorID, Temperature, Wind, Humidity, CO2) VALUES ({x+1},{sensor_data['Temperature']},{sensor_data['Wind']},{sensor_data['Humidity']}, {sensor_data['CO2']});"
            )
        conn.commit()
    except pyodbc.Error as connection_error:
        logging.info(f"Error connecting to the database: {str(connection_error)}")
    finally:
        if "cursor" in locals():
            cursor.close()
        if "conn" in locals():
            conn.close()

# Calculating statistics for each sensor and writing to sensor-statistics table
def read_data():
    string = "AVG,MAX,MIN\n"
    connection_string = "Driver={ODBC Driver 18 for SQL Server};Server=tcp:sc21sc.database.windows.net,1433;Database=sensors;Uid=CloudSA8960402f;Pwd={password};Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;"

    try:
        conn = pyodbc.connect(connection_string)
        cursor = conn.cursor()

        for metric in ["Wind", "Temperature", "Humidity", "CO2"]:
            string += metric + "\n"
            for x in range(0, 20):
                string += str(x) + ": "
                cursor.execute(
                    f"SELECT AVG([{metric}]) AS AverageWind FROM [sensor-data] WHERE [sensorID] = {x+1}"
                )
                row = cursor.fetchone()
                avg = -1
                if row:
                    avg = row[0]
                    string += str(avg) + " "

                maxn = -1
                cursor.execute(
                    f"SELECT MAX([{metric}]) AS AverageWind FROM [sensor-data] WHERE [sensorID] = {x+1}"
                )
                row = cursor.fetchone()
                if row:
                    maxn = row[0]
                    string += str(maxn) + " "

                minn = -1
                cursor.execute(
                    f"SELECT MIN([{metric}]) AS AverageWind FROM [sensor-data] WHERE [sensorID] = {x+1}"
                )
                row = cursor.fetchone()
                if row:
                    minn = row[0]
                    string += str(minn) + " "
                string += "\n"

                cursor.execute(
                    f"UPDATE [sensor-statistics] SET AVG{metric} = {avg}, MAX{metric} = {maxn}, MIN{metric} = {minn} WHERE sensorID = {x+1};"
                )

        conn.commit()

    except pyodbc.Error as connection_error:
        logging.info(f"Error connecting to the database: {str(connection_error)}")
    finally:
        if "cursor" in locals():
            cursor.close()
        if "conn" in locals():
            conn.close()
    return string


app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

# Azure Serverless Function update_sensor_data
@app.route(route="update_sensor_data", auth_level=func.AuthLevel.ANONYMOUS)
def update_sensor_data(req: func.HttpRequest) -> func.HttpResponse:
    try:
        write_data()
    except Exception:
        logging.info("Error processing request.")
        return func.HttpResponse(
            "There was a server error.",
            status_code=500,
        )
    logging.info("Statistics processed successfully.")
    return func.HttpResponse(
        "Successfully wrote data.",
        status_code=200,
    )

# Azure Serverless Function read_sensor_data
@app.route(route="read_sensor_data", auth_level=func.AuthLevel.ANONYMOUS)
def read_sensor_data(req: func.HttpRequest) -> func.HttpResponse:
    try:
        string = read_data()
    except Exception:
        logging.info("Error processing request.")
        return func.HttpResponse(
            "There was a server error.",
            status_code=500,
        )
    logging.info("Statistics processed successfully.")
    return func.HttpResponse(
        string,
        status_code=200,
    )

# Azure Serverless Function timed to emulate constant stream of data
@app.timer_trigger(
    schedule="*/5 * * * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False
)
def timer_trigger(myTimer: func.TimerRequest) -> None:
    if myTimer.past_due:
        logging.info("The timer is past due!")

    write_data()
    logging.info("Sensor data generated and written.")
